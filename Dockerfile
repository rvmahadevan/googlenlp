FROM python:3.7
MAINTAINER Mahadevan Varadhan


RUN mkdir -p /usr/local/nlp/ \
    /usr/local/data \
    /usr/local/config \
    /var/lib/mysql

# Create app directory
WORKDIR /usr/local/nlp/

# Install app dependencies
COPY /src/ /usr/local/nlp/src/
COPY requirements.txt ./
COPY /config/ /usr/local/nlp/config/

RUN pip install --upgrade pip

RUN python -m pip install grpcio
RUN python -m pip install grpcio-tools

RUN pip install -U Flask && \
    pip install Flask-RESTful && \
    pip install Flask-JWT && \
    pip install flask-bcrypt && \
    pip install Flask-Migrate && \
    pip install flask_login && \
    pip install pandas && \
    pip install nltk && \
    pip install sqlalchemy && \
    pip install psycopg2-binary && \
    pip install --upgrade werkzeug  && \
    pip install pyyaml  && \
    pip install pymysql && \
    pip install cryptography && \
    pip install simplejson  && \
    pip install jupyter


# RUN pip install google && \
#     pip install pip install google-api-core && \
#     pip install google-api-python-client && \
#     pip install google-auth  && \
#     pip install google-auth-httplib2 && \
#     pip install google-cloud && \
#     pip install google-cloud-language && \
#     pip install google-cloud-vision && \
#     pip install googleapis-common-protos


ENV GOOGLE_APPLICATION_CREDENTIALS=/usr/local/nlp/config/texttospeech.json

# ENTRYPOINT ["python"]
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]

EXPOSE 5000 8080  8888 

