import argparse
import io
import json
import os, sys
from google.cloud import language
import numpy
import six


class GoogleNLPOperation:
    """Text classification"""
    def __init__(self):
        super().__init__()

    @classmethod
    def classify(cls, text, verbose=True):
        language_client = language.LanguageServiceClient()
        document = language.types.Document(
            content=text,
            type=language.enums.Document.Type.PLAIN_TEXT)
        response = language_client.classify_text(document)
        categories = response.categories
        result = {}
        for category in categories:
            # Turn the categories into a dictionary of the form:
            # {category.name: category.confidence}, so that they can
            # be treated as a sparse vector.
            result[category.name] = category.confidence
        if verbose:
            print(text)
            for category in categories:
                # print(u'=' * 20)
                print(u'{:<16}: {}'.format('category', category.name))
                print(u'{:<16}: {}'.format('confidence', category.confidence))

        return result


tagsGeneration = GoogleNLPOperation()
text = """Bitcoin bulls have been on a bullish run, triggered by high liquidity in 
the global money markets. Investors remain bullish in the long term despite the blurred 
global economic outlook and resurgence of the COVID-19 virus."""
result = tagsGeneration.classify(text=text)
print(result)

